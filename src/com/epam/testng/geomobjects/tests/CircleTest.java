package com.epam.testng.geomobjects.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import epam.saratov.homeWork.testng.objects.GeometricObjects;
import epam.saratov.homeWork.testng.objects.GeometricObjects.Circle;

public class CircleTest {
	GeometricObjects objectCreator = new GeometricObjects();
	final double DELTA = 0.001;

	@Test(priority = 3)
	public void checkAreaCalculation() {
		Circle c = objectCreator.getCircle(0.5);
		double area = c.getSquare();
		assertEquals(area, 0.785, DELTA, "Area is not calculated correctly:");
	}

	@Parameters({"radius", "expectedCircumference"})
	@Test(priority = 2)
	public void checkCircumferenceCalculation(@Optional("15") double radius, @Optional("94.2477") double expectedCircumference) {
		
		Circle c = objectCreator.getCircle(radius);
		double circumference = c.getCircumference();
		assertEquals(circumference, expectedCircumference, DELTA, "Circumference is not calculated correctly:");
	}

	@Parameters({"radius", "expectedCircumference"})
	@Test(priority = 1)
	public void checkCircumferenceCalculationForReversedRadius(@Optional("0") double radius, @Optional("0") double expectedCircumference) {
		checkCircumferenceCalculation(-radius, expectedCircumference);
	}
}
