package com.epam.testng.geomobjects.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class SquareTestFactory extends SquareTest {

	@Factory(dataProvider = "SquareSide")
	public SquareTestFactory(double side, double expectedArea) {
		super(side, expectedArea);
	}

	@DataProvider(name = "SquareSide")
	public static Object[][] getSide() {
		return new Object[][] { new Object[] { 123,  15129}, new Object[] { -12.1, 146.41 }, new Object[] { 0, 0 }, new Object[] { 534.10012, 285262.938 } };
	}
}
