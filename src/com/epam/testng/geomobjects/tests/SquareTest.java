package com.epam.testng.geomobjects.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import epam.saratov.homeWork.testng.objects.GeometricObjects;
import epam.saratov.homeWork.testng.objects.GeometricObjects.Quadrate;

public class SquareTest {
	GeometricObjects objectCreator = new GeometricObjects();
	final double DELTA = 0.001;
	double side;
	double expectedArea;
	
	public SquareTest(double side, double expectedArea){
		this.side = side;
		this.expectedArea = expectedArea;
	}
	
	@Test(groups="Normal")
	public void checkPerimeterCalculation() {
		Quadrate q = objectCreator.getQuadrate(-2);
		double perimeter = q.getPerimeter();
		assertEquals(perimeter, 8, DELTA, "Perimeter is not calculated correctly:");
	}

	@Test(groups="Normal")
	public void checkAreaCalculation() {
		Quadrate q = objectCreator.getQuadrate(side);
		double area = q.getSquare();
		assertEquals(area, expectedArea, DELTA, "Area is not calculated correctly:");
	}
	
	@Test(dependsOnGroups="Normal")
	public void checkZeroAreaCalculation() {
		Quadrate q = objectCreator.getQuadrate(0);
		double area = q.getSquare();
		assertEquals(area, 0, DELTA, "Zero area is not calculated correctly:");
	}
}
