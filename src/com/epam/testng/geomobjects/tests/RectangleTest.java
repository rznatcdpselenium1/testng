package com.epam.testng.geomobjects.tests;

import static org.testng.Assert.*;
import org.testng.annotations.*;
import epam.saratov.homeWork.testng.objects.GeometricObjects;
import epam.saratov.homeWork.testng.objects.GeometricObjects.Rectangle;

public class RectangleTest {
	GeometricObjects objectCreator = new GeometricObjects();
	final double DELTA = 0.001;

	@Test()
	public void checkPerimeterCalculation() {
		Rectangle r = objectCreator.getRectangle(5, -3);
		double perimeter = r.getPerimeter();
		assertEquals(perimeter, 16, DELTA, "Perimeter is not calculated correctly:");
	}

	@Test()
	public void checkAreaCalculation() {
		Rectangle r = objectCreator.getRectangle(4, 0.25);
		double area = r.getSquare();
		assertEquals(area, 1, DELTA, "Area is not calculated correctly:");
	}

	@Test(dependsOnMethods = "checkPerimeterCalculation")
	public void checkTriPerimeterLOL() {
		Rectangle r = objectCreator.getRectangle(-2, -1);
		double triPerimeter = 3 * r.getPerimeter();
		assertEquals(triPerimeter, 18, DELTA, "Triple perimeter is not calculated correctly:");
	}

	@Test(dataProvider = "RectangleSidesForSquareCheck")
	public void checkSquareCheck(double firstSide, double secondSide, boolean expectedIsSquare) {
		Rectangle r = objectCreator.getRectangle(firstSide, secondSide);
		boolean isSquare = r.isQuadrate();
		assertEquals(isSquare, expectedIsSquare, "The isQuadrate method returned wrong result:");
	}

	@DataProvider(name = "RectangleSidesForSquareCheck")
	public Object[][] getSides() {
		Object[][] sides = new Object[][] { new Object[] { 1, -2, false }, new Object[] { -2, -2, true }, new Object[] { 6, 6, true },
				new Object[] { -2, -2.00001, true }, new Object[] { -4.5, 4.5, true }, new Object[] { 0, 0, true }};
		return sides;
	}

	@Test(dataProvider = "AxeBadData", expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "it sunk...")
	public void checkIllegalArgumentException(Object firstSide, Object secondSide) {
		createStringtangle(firstSide, secondSide);
	}

	private void createStringtangle(Object firstSide, Object secondSide) {
		try {
			@SuppressWarnings("unused")
			Rectangle r = objectCreator.getRectangle((double) firstSide, (double) secondSide);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("it sunk...", e);
		}
	}

	@DataProvider(name = "AxeBadData")
	public Object[][] getStringSides() {
		Object[][] sides = new Object[][] { new Object[] { "yellow", "submarine" }, };
		return sides;
	}
}
